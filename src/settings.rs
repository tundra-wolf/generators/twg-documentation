use clap::ValueEnum;
// Re-export HtmlOutputFormat
pub use twg_html::settings::HtmlOutputFormat;

/// Supported documentation output formats
#[derive(Clone, Debug, PartialEq, ValueEnum)]
pub enum DocumentationOutputFormat {
    /// AsciiDoc
    AsciiDoc,

    /// HTML5
    Html,

    /// Markdown
    Markdown,

    /// PDF
    Pdf,

    /// TROFF
    Troff,
}

impl Default for DocumentationOutputFormat {
    fn default() -> Self {
        Self::Markdown
    }
}

/// Supported page layouts for PDF and TROFF formats
#[derive(Clone, Debug, PartialEq, ValueEnum)]
pub enum DocumentationPageLayout {
    /// A3 double sided
    A3,

    /// A3 single sided
    A3SingleSided,

    /// A4 double sided
    A4,

    /// A4 single sided
    A4SingleSided,

    /// A5 double sided
    A5,

    /// A5 single sided
    A5SingleSided,

    /// US Letter double sided
    USLetter,

    /// US Letter single sided
    USLetterSingleSided,
}

impl Default for DocumentationPageLayout {
    fn default() -> Self {
        Self::A4
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn html_output_format_re_export() {
        let hof = HtmlOutputFormat::ByChapter;
        assert_eq!(HtmlOutputFormat::ByChapter, hof);
    }

    #[test]
    fn documentation_output_format() {
        let dof = DocumentationOutputFormat::AsciiDoc;
        assert_eq!(DocumentationOutputFormat::AsciiDoc, dof);
        let dof = DocumentationOutputFormat::Html;
        assert_eq!(DocumentationOutputFormat::Html, dof);
        let dof = DocumentationOutputFormat::Markdown;
        assert_eq!(DocumentationOutputFormat::Markdown, dof);
        let dof = DocumentationOutputFormat::Pdf;
        assert_eq!(DocumentationOutputFormat::Pdf, dof);
        let dof = DocumentationOutputFormat::Troff;
        assert_eq!(DocumentationOutputFormat::Troff, dof);

        let default_dof = DocumentationOutputFormat::default();
        assert_eq!(DocumentationOutputFormat::Markdown, default_dof);
    }

    #[test]
    fn documentation_page_layout() {
        let dpl = DocumentationPageLayout::A3;
        assert_eq!(DocumentationPageLayout::A3, dpl);
        let dpl = DocumentationPageLayout::A3SingleSided;
        assert_eq!(DocumentationPageLayout::A3SingleSided, dpl);
        let dpl = DocumentationPageLayout::A4;
        assert_eq!(DocumentationPageLayout::A4, dpl);
        let dpl = DocumentationPageLayout::A4SingleSided;
        assert_eq!(DocumentationPageLayout::A4SingleSided, dpl);
        let dpl = DocumentationPageLayout::A5;
        assert_eq!(DocumentationPageLayout::A5, dpl);
        let dpl = DocumentationPageLayout::A5SingleSided;
        assert_eq!(DocumentationPageLayout::A5SingleSided, dpl);
        let dpl = DocumentationPageLayout::USLetter;
        assert_eq!(DocumentationPageLayout::USLetter, dpl);
        let dpl = DocumentationPageLayout::USLetterSingleSided;
        assert_eq!(DocumentationPageLayout::USLetterSingleSided, dpl);

        let default_dpl = DocumentationPageLayout::default();
        assert_eq!(DocumentationPageLayout::A4, default_dpl)
    }
}
